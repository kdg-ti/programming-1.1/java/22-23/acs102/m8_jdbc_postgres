package pro11.m8.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class TestJdbc {

  public static void main(String[] args) {
    Connection cx;

    {
      try {
        cx = DriverManager.getConnection("jdbc:postgresql://localhost:5432/Enterprise",
            "postgres","Student_1234" );
        Statement statement = cx.createStatement();
        int rows=statement.executeUpdate(
            "INSERT INTO departments VALUES (90, 'Java Development',NULL,NULL)"
        );
        System.out.println(rows + " rows updated");
      } catch (SQLException e) {
        System.err.println("*** Database error ***");
        e.printStackTrace();
      }
    }
  }

}
